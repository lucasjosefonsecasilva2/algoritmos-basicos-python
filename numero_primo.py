# Numero primo

num_inicial = int(input("Numero inicial: "))
num_final = int(input("Numero final: "))
intervalo = int(input("Intervalo: "))
list_primos = []
list_nprimos = []
x = 0 # Vai armazenar quantidade de numeros primos.
y = 0 # Vai armazenar quantidade de numeros nao primos.

for c in range(num_inicial,num_final,intervalo): #Loop com o intervalo informado
    dv = 0
    for i in range(1,c): # Procura por divisores.
        # print("%i modulo %i E %i "%(c,i,c%i))
        if(c%i==0):
            dv += 1
            if(dv>1):
                y += 1
                list_nprimos.append(c)
                break # Se exitir mais de um divisor, nao e primo.
    if(dv == 1): # Apenas um divisor entre 1 e o numero informado, entao e primo.
        list_primos.append(c) # Armazena numeros primos em uma lista.
        x += 1

print("\n%i numeros primos encontrados: "%x)
print(list_primos)
print("\n%i numeros nao primos: "%y)
print(list_nprimos)
